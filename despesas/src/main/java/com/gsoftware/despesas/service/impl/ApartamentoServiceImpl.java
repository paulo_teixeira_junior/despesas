package com.gsoftware.despesas.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsoftware.despesas.model.Apartamento;
import com.gsoftware.despesas.repository.ApartamentoRepository;
import com.gsoftware.despesas.service.ApartamentoService;

@Service
public class ApartamentoServiceImpl implements ApartamentoService {

	@Autowired
	private ApartamentoRepository apartamentoRepository;
	
	@Override
	public List<Apartamento> getAll() {
		return apartamentoRepository.findAll();
	}

	@Override
	public Apartamento getApartamento(Long id) {
		return apartamentoRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Apartamento apartamento) {
		apartamentoRepository.save(apartamento);
	}

	@Override
	public void update(Long id, Apartamento apartamento) {
		Apartamento entity = apartamentoRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = apartamento;
			entity.setId(id);
			apartamentoRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		apartamentoRepository.deleteById(id);
	}

	@Override
	public Double calcularValorCondominio(Apartamento apartamento) {
		return (apartamento.getBloco().getGastos() / apartamento.getBloco().getArea()) * apartamento.getArea();
	}

}
