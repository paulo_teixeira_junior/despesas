package com.gsoftware.despesas.service;

import java.util.List;

import com.gsoftware.despesas.model.Condominio;

public interface CondominioService {
	
	public List<Condominio> getAll();
	
	public Condominio getCondominio(Long id);
	
	void save(Condominio condominio);
	
	void update(Long id, Condominio condominio);
	
	void delete(Long id);
}
