package com.gsoftware.despesas.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsoftware.despesas.model.Condominio;
import com.gsoftware.despesas.repository.CondominioRepository;
import com.gsoftware.despesas.service.CondominioService;

@Service
public class CondominioServiceImpl implements CondominioService {

	@Autowired
	private CondominioRepository condominioRepository;
	
	@Override
	public List<Condominio> getAll() {
		return condominioRepository.findAll();
	}

	@Override
	public Condominio getCondominio(Long id) {
		return condominioRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Condominio condominio) {
		condominio.getBlocos().stream().forEach(bl -> bl.setCondominio(condominio));
		condominioRepository.save(condominio);
	}

	@Override
	public void update(Long id, Condominio condominio) {
		Condominio entity = condominioRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = condominio;
			entity.setId(id);
			entity.getBlocos().stream().forEach(bl -> bl.setCondominio(condominio));
			condominioRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		condominioRepository.deleteById(id);
	}

}
