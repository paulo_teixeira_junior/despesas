package com.gsoftware.despesas.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsoftware.despesas.model.Bloco;
import com.gsoftware.despesas.repository.BlocoRepository;
import com.gsoftware.despesas.service.BlocoService;

@Service
public class BlocoServiceImpl implements BlocoService {

	@Autowired	private BlocoRepository blocoRepository;
	
	@Override
	public List<Bloco> getAll() {
		return blocoRepository.findAll();
	}

	@Override
	public Bloco getBloco(Long id) {
		return blocoRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Bloco bloco) {
		bloco.getApartamentos().stream().forEach(ap -> ap.setBloco(bloco));
		blocoRepository.save(bloco);
	}

	@Override
	public void update(Long id, Bloco bloco) {
		Bloco entity = blocoRepository.findById(id).orElse(null);
		if(entity != null) {
			entity = bloco;
			entity.setId(id);
			entity.getApartamentos().stream().forEach(ap -> ap.setBloco(bloco));
			blocoRepository.save(entity);
		}
	}

	@Override
	public void delete(Long id) {
		blocoRepository.deleteById(id);
	}

}
