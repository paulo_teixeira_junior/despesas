package com.gsoftware.despesas.service;

import java.util.List;

import com.gsoftware.despesas.model.Apartamento;

public interface ApartamentoService {

	public List<Apartamento> getAll();
	
	public Apartamento getApartamento(Long id);
	
	void save(Apartamento apartamento);
	
	void update(Long id, Apartamento apartamento);
	
	void delete(Long id);
	
	public Double calcularValorCondominio(Apartamento apartamento);
	
}
	
	
