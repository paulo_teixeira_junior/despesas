package com.gsoftware.despesas.service;

import java.util.List;

import com.gsoftware.despesas.model.Apartamento;
import com.gsoftware.despesas.model.Bloco;

public interface BlocoService {

	public List<Bloco> getAll();
	
	public Bloco getBloco(Long id);
	
	void save(Bloco bloco);
	
	void update(Long id, Bloco bloco);
	
	void delete(Long id);

}
