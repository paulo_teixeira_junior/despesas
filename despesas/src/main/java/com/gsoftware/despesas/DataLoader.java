package com.gsoftware.despesas;

import java.util.Arrays;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.gsoftware.despesas.model.Apartamento;
import com.gsoftware.despesas.model.Bloco;
import com.gsoftware.despesas.model.Condominio;
import com.gsoftware.despesas.service.ApartamentoService;
import com.gsoftware.despesas.service.CondominioService;

@Component
public class DataLoader implements CommandLineRunner {

	@Autowired
	private ApartamentoService apartamentoService;
	
	@Autowired
	private CondominioService condominioService;
	
	@Override
	public void run(String... args) throws Exception {
        Bloco bloco = new Bloco(1150.5d, 7500.0d);
        Apartamento apartamento = new Apartamento(45.6d, bloco);
        bloco.setApartamentos(Arrays.asList(apartamento));
        Condominio condominio = new Condominio(Arrays.asList(bloco));

        condominioService.save(condominio);
        Apartamento apto = apartamentoService.getAll().get(0);
        System.out.println("O Valor do condominio para o apartamento digitado é: R$" + String.format("%.2f", apartamentoService.calcularValorCondominio(apto)));     
	}

}
