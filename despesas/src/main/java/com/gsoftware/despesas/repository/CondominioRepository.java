package com.gsoftware.despesas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gsoftware.despesas.model.Condominio;

@Repository
public interface CondominioRepository extends JpaRepository<Condominio, Long> {

}
