package com.gsoftware.despesas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gsoftware.despesas.model.Bloco;

@Repository
public interface BlocoRepository extends JpaRepository<Bloco, Long> {

}
